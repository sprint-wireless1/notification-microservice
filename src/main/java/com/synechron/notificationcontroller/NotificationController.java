package com.synechron.notificationcontroller;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("api/notifications")
public class NotificationController {
	
	@GetMapping
	public String notifications() {
		return "notification service";
	}
	 

}